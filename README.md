# Blank README.md

A script that puts text from files ending with `.txt` from various subdirectories in one text file


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Just clone this project.

```
git clone git@gitlab.com:cherrysushi/files-in-subdirs-_bash_.git
```

or

```
git clone https://gitlab.com/cherrysushi/files-in-subdirs-_bash_.git
```

### Installing

Give execution rights.

```
chmod +x script
```

### Running

Running the project.

```
./script
```

## Authors

cherrysushi *Initial work* - [gitlab](https://gitlab.com/cherrysushi)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

